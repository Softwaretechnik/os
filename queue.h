/* ------------------------------------------------------------------------- */
/* Datei:              queue.h                                               */
/* Autor:              Marcus Meiburg, Hannes Range                          */
/* Version:            1.5                                                   */
/* letzte Bearbeitung: 07.06.2013                                            */
/* Beschreibung:       Headerdatei der FIFO Warteschlange "queue.c"          */
/* ------------------------------------------------------------------------- */

#ifndef __QUEUE__
#define __QUEUE__

/* Includes */
#include <stdio.h>
#include <stdlib.h>

/* Maximaler Zugriffswert den ein Prozess warten muss bis auf jeden Fall     */
/* versucht wird ihn zu bearbeiten                                           */
#define MAX_ACCESS 15

/* ------------------------------------------------------------------------- */
/*                 Repraesentation einer Zelle der Queue                     */
/* ------------------------------------------------------------------------- */
struct queue_zelle {
   unsigned pid;             // Prozess ID
   unsigned size;            // Prozess Groesse
   unsigned access;          // Anzahl der Zugriffe
   struct queue_zelle *next; // Zeiger auf die naechste Zelle
   struct queue_zelle *prev; // Zeiger auf die vorherige Zelle
};
typedef struct queue_zelle qZelle; 

/* ------------------------------------------------------------------------- */
/*                         Prototypenvereinbarung                            */
/* ------------------------------------------------------------------------- */
/* Initialiesierungs Funktion ACHTUNG!: muss als erstes Implementiert werden */
Boolean queue_Init(void);
/* Prueft ob die Queue leer ist */
Boolean queue_IsEmpty();
/* Versucht der Queue ein Element hinzuzufuegen */
Boolean queue_enQueue(unsigned pid, unsigned size);
/* Gibt die pID des ersten Elements aus und loescht es aus der Queue */
unsigned queue_deQueue();
/* Gibt das erste Element als struct zurueck */
qZelle *queue_GetFirst();
/* Schiebt die aktuelle Zelle an den Anfang der Queue */
void queue_CellToFirst(qZelle* cell);
/* Gibt das beste passende Element zurueck */
qZelle *queue_GetBestCandidate();
/* Ausgabefunktion zum Testen der Funktionalitaet */
void queue_Print();

#endif /* __QUEUE__ */