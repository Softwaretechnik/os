/* ------------------------------------------------------------------------- */
/* Datei:              queue.cpp                                             */
/* Autor:              Marcus Meiburg, Hannes Range                          */
/* Version:            1.5                                                   */
/* letzte Bearbeitung: 07.06.2013                                            */
/* Beschreibung:       Implementation einer FIFO Warteschlange mit Zugriffs- */
/*                     zaehler                                               */
/* ------------------------------------------------------------------------- */

#include "globals.h"
#include "queue.h"

/* ------------------------------------------------------------------------- */
/*                       Globale Variablen                                   */
/* ------------------------------------------------------------------------- */
/* Anfang der Warteschlange */
qZelle *qAnfang;

/* ------------------------------------------------------------------------- */
/*                       Initialisierung                                    */
/* ------------------------------------------------------------------------- */
Boolean queue_Init(void) {
  /* Mehrfaches Initialisieren vermeiden*/
  if(qAnfang == NULL) {
    if((qAnfang=(qZelle*)malloc(sizeof(qZelle))) != NULL)  {
      qAnfang->pid = 0;
      qAnfang->size = 0;
      qAnfang->prev = qAnfang;
      qAnfang->next = qAnfang;
      return TRUE;
    }
  }
  return FALSE;
}

/* Pruefft ob die Warteschlange leer ist*/
Boolean queue_IsEmpty() {
  if(qAnfang->next == qAnfang)
      return TRUE;
  else
      return FALSE;
}

/* ------------------------------------------------------------------------- */
/*                      Hinzufuegen einer Zelle                              */
/* ------------------------------------------------------------------------- */
Boolean queue_enQueue(unsigned pid, unsigned size) {
  qZelle *neuZelle; // Neu einzufuegende Zelle

  if((neuZelle = (qZelle*)malloc(sizeof(qZelle))) != NULL) {
    /* Zelle immer vor dem "qAnfang"(->prev) einfuegen */
    neuZelle->prev = qAnfang->prev;
    neuZelle->prev->next = neuZelle;
    neuZelle->next = qAnfang;
    qAnfang->prev = neuZelle;
    neuZelle->pid = pid;
    neuZelle->size = size;
    neuZelle->access = 0;
    return TRUE;
  }
  return FALSE;
}

/* ------------------------------------------------------------------------- */
/*                      Loeschen einer Zelle                                 */
/* ------------------------------------------------------------------------- */
unsigned queue_deQueue() {
  unsigned pid;    // speichert die Pid der zu loeschende Zelle
  qZelle* helper;  // Zeiger auf die zu loeschende Zelle

  /* Wenn die Warteschlange nicht leer ist pid des erstes Elements ausgeben */
  if(!queue_IsEmpty()) {
      /* Zelle nach "qAnfang"8->next) auslesen und loeschen */
      helper = qAnfang->next;
      pid = helper->pid;
      qAnfang->next = helper->next;
      helper->next->prev = qAnfang;
      free(helper);
      return pid;
  } else {
      logGeneric("Warteschlange ist leer");
  }
  return 0;
}

/* ------------------------------------------------------------------------- */
/*            Gibt die beste Zelle zum einlagern zurueck                     */
/* ------------------------------------------------------------------------- */
qZelle *queue_GetBestCandidate() {
  qZelle* zeiger = qAnfang->next;       /* zeiger zum durchwandern der Queue */
  qZelle* nextElement = qAnfang->next;  /* zeiger als Rueckgabe der Funktion */
  unsigned minSize = UINT_MAX;          /* aktuelle minimale Size der Queue  */
  unsigned maxAccess = MAX_ACCESS;      /* aktueller maximaler Zugriff  ""   */
  Boolean check = TRUE;                 /* wurde schon eine Zelle gefunden ? */

  if(!queue_IsEmpty()) {
    /* Zugriff bei allem Elementen erhoehen */
    zeiger = qAnfang->next;
    while(zeiger != qAnfang) {
      zeiger->access++;
      zeiger = zeiger->next;
    }
    zeiger = qAnfang->next;
    while(zeiger != qAnfang) {
      /* Bestimmt die Zelle mit den meisten Zugriffen */
      if(zeiger->access > maxAccess) {
          maxAccess = zeiger->access;
          nextElement = zeiger;
          check = FALSE;
      /* Bestimme die kleinste Zelle wenn nicht schon eine */
      /* mit mehr Zugriffen gefunden wurde                 */
      } else if(zeiger->size < minSize && check) {
          minSize = zeiger->size;
          nextElement = zeiger;
      }
      zeiger = zeiger->next;
    }
  }

  /* Verschiebt das gefundene Element an den Anfang der Queue */
  if(nextElement != qAnfang->next) {
    queue_CellToFirst(nextElement);
  }
  /* Gibt die erste Zelle zurueck falls es kein Ergebnis gibt */
  return nextElement;
}

/* ------------------------------------------------------------------------- */
/*            Schiebt eine Zelle an den Anfang der Queue                     */
/* ------------------------------------------------------------------------- */
void queue_CellToFirst(qZelle* cell) {
  qZelle* vorgaenger = cell->prev;
  qZelle* nachfolger = cell->next;

  cell->next = qAnfang->next;
  qAnfang->next->prev = cell;
  qAnfang->next = cell;
  cell->prev = qAnfang;

  vorgaenger->next = nachfolger;
  if(nachfolger != NULL)
    nachfolger->prev = vorgaenger;
}

/* ------------------------------------------------------------------------- */
/*            Gibt das erste Element der Queue als Struktur aus              */
/* ------------------------------------------------------------------------- */
qZelle *queue_GetFirst() {
  if(!queue_IsEmpty()) {
    return qAnfang->next;
  }
  return NULL;
}

/* Gibt alle Speicherzellen in einer Tabelle aus */
void queue_Print() {
  qZelle* zeiger = qAnfang->next;
  printf("-------- Queue ---------\n");
  printf("------------------------\n");
  printf("| PID |  Size | Access |\n");
  printf("------------------------\n");
  while(zeiger != qAnfang) {
    printf("| %3u | %5u | %6u |\n",zeiger->pid, zeiger->size,zeiger->access);
    zeiger = zeiger->next;
  }
  printf("------------------------\n");
}