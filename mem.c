/* ------------------------------------------------------------------------- */
/* Datei:              mem.cpp                                               */
/* Autor:              Hannes Range, Marcus Meiburg                          */
/* Version:            1.8                                                   */
/* letzte Bearbeitung: 07.06.2013                                            */
/* Beschreibung:       Implementation einer Verketteten Liste mit            */
/*                     Kompaktierung. Einfuegen mit First Fit Methode        */
/* ------------------------------------------------------------------------- */

#include "globals.h"
#include "mem.h"

/* ------------------------------------------------------------------------- */
/*                       Globale Variablen                                   */
/* ------------------------------------------------------------------------- */
/* Anfang der verketteten Liste*/
mZelle* mAnfang = NULL;

/* ------------------------------------------------------------------------- */
/*                       Initialiesierung                                    */
/* ------------------------------------------------------------------------- */
Boolean mem_Init() {
  if(mAnfang == NULL) {
    if((mAnfang =(mZelle*)malloc(sizeof(mZelle)))!= NULL) {
      mAnfang->next = NULL;
      mAnfang->prev = NULL;
      /* Zelleninhalt bestimmen */
      mAnfang->type = LUECKE;
      mAnfang->pos = 0;
      mAnfang->size = MEMORY_SIZE;
      mAnfang->pid = 0;
      return TRUE;
    }
  }
  return FALSE;
}

/* ------------------------------------------------------------------------- */
/*                   Loeschen eines Prozesses                                */
/* ------------------------------------------------------------------------- */
Boolean mem_remProcess(unsigned pid) {
  mZelle *akt_zelle = NULL;
  akt_zelle = mAnfang;
  /* Verkettete Liste durchgehen bis Prozess gefunden od. am Ende angekommen */
  while(akt_zelle->next != NULL && akt_zelle->pid != pid) {
    akt_zelle = akt_zelle->next; 
  }
  /* Wurde der Prozess gefunden */
  if(akt_zelle->pid == pid) {
    /* Prozess wird zur Luecke */
    akt_zelle ->type = LUECKE;
    akt_zelle->pid = 0;
    /* Verschmelzung durchfuehren */
    mem_merge(akt_zelle);
    return TRUE;
  }
  return FALSE;
}

/* ------------------------------------------------------------------------- */
/*                          Verschmelzen                                     */
/* ------------------------------------------------------------------------- */
void mem_merge(mZelle *akt_zelle) {
  mZelle *vorgaenger = NULL, *nachfolger = NULL;

  vorgaenger = akt_zelle->prev;
  nachfolger = akt_zelle->next;

  /* Verschmelzen zur rechten Seite */
  /* Pruefe ob es einen Nachfolger gibt */
  if(nachfolger != NULL) {
    /* Ist der VOrgaenger eine Luecke */
    if(nachfolger->type == LUECKE) {
      /* Anpassen der Zellen Informationen */
      akt_zelle->size = akt_zelle->size + nachfolger->size;
      /* Hat der Nachfolger noch einen Nachfolger */
      if(nachfolger->next != NULL) {
          /* Nachfolger des Nachfolgers mit der aktuellen Zelle verknuepfen */
          nachfolger->next->prev = akt_zelle;
          /* aktuelle Zelle auf den neuen Nachfolger zeigen lassen */
          akt_zelle->next = nachfolger->next;
      } else {
          akt_zelle->next = NULL;
      }
      /* Speicherzelle der Luecke freigeben */
      free(nachfolger);
    }
  }

  /* Verschmelzen zur linken Seite    */
  /* Pruefe ob es einen Vorgaenger gibt */
  if(vorgaenger != NULL) {
    /* Ist der Vorgaenger eine Luecke */
    if(vorgaenger->type == LUECKE) {
      /* Anpassen der Zellen Informationen */
      akt_zelle->pos = vorgaenger->pos;
      akt_zelle->size = akt_zelle->size + vorgaenger->size;
      /* Hat der Vorgaenger noch weitere Vorgaenger */
      if(vorgaenger->prev != NULL) {
          /* Vorgaenger des Vorgaengers mit der aktuellen Zelle verknuepfen */
          vorgaenger->prev->next = akt_zelle;
          /* aktuelle Zelle auf den neuen Vorgaenger zeigen lassen */
          akt_zelle->prev = vorgaenger->prev;
      /* Aktuelle Zelle wird neuer Anfang */
      } else {
          akt_zelle->prev = NULL;
          mAnfang = akt_zelle;
      }
      /* Speicherzelle der Luecke freigeben */
      free(vorgaenger);
    }
  }
}

/* ------------------------------------------------------------------------- */
/*                  Hinzufuegen eines Prozesses                              */
/* ------------------------------------------------------------------------- */
Boolean mem_addProcess(unsigned pid, unsigned size) {
  mZelle *zeiger = NULL, *neuZelle = NULL, *nachfolger = NULL;

  zeiger = mAnfang;
  while(zeiger->next != NULL) {
    /* Erste Luecke mit genug freiem Speicher suchen (FirstFit) */
    if(zeiger->size >= size && zeiger->type == LUECKE) {
      break;
    }
     zeiger = zeiger->next;
  }

  /* Verschmelzen wenn keine Luecke gefunden wurde, die Gro� genug ist */
  if(MEMORY_SIZE - usedMemory >= size && zeiger->next == NULL && zeiger != mAnfang) {
    zeiger = mem_kompaktierung();
  }

  /* Keine Luecke gefunden die Gro� genug ist*/
  if(zeiger->type != LUECKE || (zeiger->type == LUECKE && zeiger->size < size))
    return FALSE;

  /* Nachfolger der Luecke speichern */
  nachfolger = zeiger->next;

  /* Hat die gefundene Zelle die exakte groesse */
  if(zeiger->size == size) {
      zeiger->pid = pid;
      zeiger->type = PROZESS;
      return TRUE;
  /* Die Zelle ist groesser */
  } else if(zeiger->size > size) {
      /* Neue Zelle nach dem Zeiger einfuegen */
      if((neuZelle = (mZelle*)malloc(sizeof(mZelle))) != NULL) { 
          neuZelle->next = nachfolger;
          neuZelle->prev = zeiger;
          zeiger->next = neuZelle;
          /* Falls es einen Nachfolger gibt, den Vorgaeger anpassen */
          if(nachfolger != NULL) {
            nachfolger->prev = neuZelle;
          }
          zeiger->size = zeiger->size - size;
          /* Zelleninhalt bestimmen */
          neuZelle->pos = zeiger->size + zeiger->pos;
          neuZelle->size = size;
          neuZelle->pid = pid;
          neuZelle->type = PROZESS;
          return TRUE;
      } else {
          //logGeneric("Kein Speicherplatz mehr vorhanden!");
          return FALSE;
      }
  }
  return FALSE;
}

/* ------------------------------------------------------------------------- */
/*                              Kompaktierung                                */
/* ------------------------------------------------------------------------- */
mZelle* mem_kompaktierung() {
  mZelle *zeiger = NULL;     /* Zeiger auf die naechste Luecke */
  mZelle *vorgaenger = NULL; /* Zeiger auf den Vorgaenger der Luecke */
  mZelle *nachfolger = NULL; /* Zeiger auf den Nachfolger der Luecke */
  zeiger = mAnfang;

  /* Nur ausfuehren wenn es mehr als einen Eintrag gibt */
  if(mAnfang->next != NULL) {
    /* Ausfuehren bis die Liste einmal durchlaufen wurde, oder wenn die Luecke die maximale Groesse hat*/
    while(zeiger != NULL && (zeiger->type == LUECKE && zeiger->size < MEMORY_SIZE - usedMemory)) {
      if(zeiger->type == LUECKE) {
        nachfolger = zeiger->next;
        vorgaenger = zeiger->prev;
        
        nachfolger->prev = vorgaenger;

        /* Wenn es einen Vorgaenger gibt diesen verknuepfen */
        if(vorgaenger != NULL) {
            vorgaenger->next = nachfolger;
        /* andernfalls ist der Nachfolger der neue Anfang */
        } else {
            mAnfang = nachfolger;
        }

        nachfolger->pos = zeiger->pos;
        zeiger->pos = nachfolger->pos + nachfolger->size;
        nachfolger->next->prev = zeiger;
        zeiger->next = nachfolger->next;
        nachfolger->next = zeiger;
        zeiger->prev = nachfolger;

        /* Wenn zwei Luecken nebeneinander sind, dann verschmelzen */
        if(zeiger->next->type == LUECKE) {
          mem_merge(zeiger);
        }
      }
      zeiger = zeiger->next;
    }
  }
  logGeneric("Kompaktierung wurde durchgefuehrt");
  /* Gibt die Luecke als Zeiger zurueck */
  return zeiger->prev;
}

/* Gibt alle Speicherzellen in einer Tabelle aus*/
void mem_Print() {
  mZelle* zeiger = mAnfang;
  printf("| PID |   Type  |   Pos. |   Size |   MyAddress | PrevAddress | NextAddress |\n");
  printf("-----------------------------------------------------------------------------\n");
  while(zeiger != NULL) {
    printf("|%5u| %s | %6d | %6d | %11p | %11p | %11p |\n",zeiger->pid, zeiger->type ? "Prozess" : "Luecke " ,
           zeiger->pos , zeiger->size, zeiger, zeiger->prev,zeiger->next);
    zeiger = zeiger->next;
  }
  printf("-----------------------------------------------------------------------------\n");
}