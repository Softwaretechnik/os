/* ------------------------------------------------------------------------- */
/* Datei:              mem.h                                                 */
/* Autor:              Hannes Range, Marcus Meiburg                          */
/* Version:            1.5                                                   */
/* letzte Bearbeitung: 07.06.2013                                            */
/* Beschreibung:       Headerdatei der doppelt verk. Liste "mem.c"           */
/* ------------------------------------------------------------------------- */

#ifndef _MEM_
#define _MEM_

/* Includes */
#include <stdio.h>
#include <stdlib.h>

/* Repraesentation des Prozesstyps */
typedef enum {LUECKE=0, PROZESS} MemoryProcessType_t;

/* ------------------------------------------------------------------------- */
/*          Repraesentation einer Zelle der doppelt verketteten Liste        */
/* ------------------------------------------------------------------------- */
struct mem_zelle {
  unsigned pid;             /* Prozess ID (Einzigartzig vom OS vergeben) */
  MemoryProcessType_t type; /* PROZESS , LUECKE (freier Speicherplatz) */
  unsigned pos;             /* Anfangsposition */
  unsigned size;            /* Gibt die Groesse des Prozesses/Luecke an */
  struct mem_zelle* next;   /* N�chste Zelle */
  struct mem_zelle* prev;   /* Vorherige Zelle */
};
typedef struct mem_zelle mZelle; 

/* ------------------------------------------------------------------------- */
/*                         Prototypenvereinbarung                            */
/* ------------------------------------------------------------------------- */
/* Initialisierungs Funktion ACHTUNG!: muss als erstes Implementiert werden */
Boolean mem_Init();
/* Fuegt einen Prozess zur Speicherverwaltung hinzu */
Boolean mem_addProcess(unsigned pid, unsigned size);
/* Loescht einen Prozess aus der Speicherverwaltung */
Boolean mem_remProcess(unsigned pid);
/* Verschmilzt benachtbarte Luecken miteinander */
void mem_merge(mZelle *akt_zelle);
/* Partitioniert die Speicherverwaltung*/
mZelle* mem_kompaktierung();
/* Ausgabefunktion zum Testen der Funktionalitaet */
void mem_Print();

#endif